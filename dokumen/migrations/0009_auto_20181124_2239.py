# Generated by Django 2.1.1 on 2018-11-24 15:39

from django.db import migrations, models
import dokumen.models


class Migration(migrations.Migration):

    dependencies = [
        ('dokumen', '0008_auto_20181124_2236'),
    ]

    operations = [
        migrations.AlterField(
            model_name='berkas',
            name='berkas',
            field=models.FileField(upload_to=dokumen.models.PathForFileModel('uploads/'), verbose_name='Berkas'),
        ),
    ]
