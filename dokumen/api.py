from tastypie.resources import ModelResource
from dokumen.models import Berkas
from tastypie.authentication import BasicAuthentication

class BerkasResource(ModelResource):
	class Meta:
		excludes = ('keterangan',)
		queryset = Berkas.objects.all()
		resource_name = 'berkas'
		authentication = BasicAuthentication()