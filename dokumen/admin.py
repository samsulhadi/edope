from django.contrib import admin
from dokumen.models import Berkas, Pegawai
from django.utils.safestring import mark_safe

# Register your models here.

class BerkasAdmin(admin.ModelAdmin):
	def aksi(self, obj):
		return mark_safe("<a href='"+str(obj.get_file_url())+"'>Download</a>")
	aksi.short_description = 'Aksi'

	def upload_oleh(self, obj):
		return obj.upload_by.get_full_name()
	upload_oleh.short_description = 'Diupload Oleh'
	upload_oleh.admin_order_field = 'upload_by'

	list_display = ('nama', 'keterangan', 'upload_oleh', 'aksi')
	# exclude = ('keterangan',)

	def get_list_display(self, request):
		if not request.user.is_superuser:
			return ('nama', 'keterangan', 'aksi')
		return super().get_list_display(request)

	def get_exclude(self, request, obj=None):
		if not request.user.is_superuser or obj is None:
			return ['upload_by',]
		return super().get_exclude(request, obj)

	def get_queryset(self, request):
		qs = super().get_queryset(request)
		if request.user.is_superuser:
			return qs
		return qs.filter(upload_by=request.user)

	def save_model(self, request, obj, form, change):
		if not change:
			obj.upload_by = request.user
		super().save_model(request, obj, form, change)

admin.site.register(Berkas, BerkasAdmin)

from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from django import forms
admin.site.unregister(User)
admin.site.unregister(Group)

class CustomUserCreationForm(UserCreationForm):
	username = forms.CharField(label="Nomor Identitas", widget=forms.TextInput(attrs={'placeholder': 'Masukkan NIP atau NIK', 'class': 'vTextField'}))

class CustomUserChangeForm(forms.ModelForm):
	first_name = forms.CharField(label="Nama Lengkap", max_length=200, widget=forms.TextInput(attrs={'placeholder': 'Nama Lengkap', 'class': 'vTextField'}))
	username = forms.CharField(label="Nomor Identitas", widget=forms.TextInput(attrs={'placeholder': 'Masukkan NIP atau NIK', 'class': 'vTextField'}))
	is_superuser = forms.BooleanField(label='Apakah admin atau pegawai biasa?', required=False)
	is_staff = forms.BooleanField(label='Apakah Aktif (bisa login jika aktif)?', required=False)
	class Meta:
		model = User
		exclude = ('password',)

class PegawaiForm(forms.ModelForm):
	first_name = forms.CharField(label="Nama Lengkap", widget=forms.TextInput(attrs={'placeholder': 'Nama Lengkap', 'class': 'vTextField'}))
	username = forms.CharField(label="Nomor Identitas", widget=forms.TextInput(attrs={'placeholder': 'Masukkan NIP atau NIK', 'class': 'vTextField'}))
	is_superuser = forms.BooleanField(label='Apakah admin atau pegawai biasa?', required=False)
	is_staff = forms.BooleanField(label='Apakah Aktif (bisa login jika aktif)?', required=False)
	class Meta:
		model = Pegawai
		exclude = ('password',)

class CustomUserAdmin(UserAdmin):
	def nomor_identitas(self, obj):
		return obj.username
	nomor_identitas.short_description = 'Nomor Identitas (NIP / NIK)'
	nomor_identitas.admin_order_field = 'username'

	def nama_lengkap(self, obj):
		return obj.first_name
	nama_lengkap.short_description = 'Nama Lengkap'
	nama_lengkap.admin_order_field = 'username'

	def is_admin(self, obj):
		return obj.is_superuser
	is_admin.short_description = 'Apakah Admin?'
	is_admin.admin_order_field = 'is_superuser'
	is_admin.boolean = True

	def is_staff(self, obj):
		return obj.is_staff
	is_staff.short_description = 'Apakah Aktif (bisa login jika aktif)?'
	is_staff.admin_order_field = 'is_staff'
	is_staff.boolean = True

	list_display = ('nomor_identitas', 'nama_lengkap', 'is_admin', 'is_staff')

	fieldsets = (None, {'fields': ('first_name', 'username', 'is_superuser', 'is_staff', 'user_permissions')}),
	add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('first_name', 'username', 'password1', 'password2', 'is_superuser', 'is_staff', 'user_permissions'),
        }),
    )
	form = CustomUserChangeForm
	add_form = CustomUserCreationForm

admin.site.register(User, CustomUserAdmin)

class PegawaiAdmin(admin.ModelAdmin):
	def nomor_identitas(self, obj):
		return obj.username
	nomor_identitas.short_description = 'Nomor Identitas (NIP / NIK)'
	nomor_identitas.admin_order_field = 'username'

	def nama_lengkap(self, obj):
		return obj.first_name
	nama_lengkap.short_description = 'Nama Lengkap'
	nama_lengkap.admin_order_field = 'username'

	def is_admin(self, obj):
		return obj.is_superuser
	is_admin.short_description = 'Apakah Admin?'
	is_admin.admin_order_field = 'is_superuser'
	is_admin.boolean = True

	def is_staff(self, obj):
		return obj.is_staff
	is_staff.short_description = 'Apakah Aktif (bisa login jika aktif)?'
	is_staff.admin_order_field = 'is_staff'
	is_staff.boolean = True

	list_display = ('nomor_identitas', 'nama_lengkap', 'is_admin', 'is_staff')
	fields = ('first_name', 'username', 'is_superuser', 'is_staff', 'user_permissions')
	form = PegawaiForm
	filter_horizontal = ('user_permissions',)

# admin.site.register(Pegawai, PegawaiAdmin)

