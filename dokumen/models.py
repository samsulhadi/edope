from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
# Create your models here.
from django.utils.deconstruct import deconstructible
from uuid import uuid4
import os

@deconstructible
class PathForFileModel(object):

	def __init__(self, sub_path):
		self.path = sub_path

	def __call__(self, instance, filename):
		ext = filename.split('.')[-1]
		# set filename as random string
		filename = '{}.{}'.format(uuid4().hex, ext)
		# return the whole path to the file
		return os.path.join(self.path, filename)

class Pegawai(User):
	class Meta:
		verbose_name='Pegawai'
		verbose_name_plural='Pegawai'

class Berkas(models.Model):
	nama = models.CharField(verbose_name="Nama Berkas", max_length=100)
	berkas = models.FileField(verbose_name="Berkas", upload_to=PathForFileModel('uploads/'))
	keterangan = models.CharField(verbose_name="Keterangan Berkas", max_length=255, blank=True, null=True)
	upload_by = models.ForeignKey(User, verbose_name="Diupload oleh", on_delete=models.PROTECT)

	def get_file_url(self):
		if self.berkas:
			return settings.MEDIA_URL+str(self.berkas)
		return "#"

	def __str__(self):
		return (str(self.id)+". "+self.nama)

	class Meta:
		verbose_name = 'Data Berkas'
		verbose_name_plural = "Data Berkas"